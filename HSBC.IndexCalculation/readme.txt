1. I have chosen to use angularjs as part of the ui due to how easy it is to create maintainable javascript, and the built in functionality such as binding and http requests.

2. The solution has been split into only 3 projects for simplicity, however there could be room to add other projects that could make maintainability and component replacement a lot easier. 

3. I have used unity for dependency injection, this is always a good idea to create loosly coupled objects and solutions. This layer of abstraction also allows us to switch out components easily. 

4. I am using web API 2 with ajax requests, both via angular $http and also jquery $.ajax to create a rich UI, however there are page redirects between views. To make the user experience better we could create a single page app with javascript and UI templates so that the website does not refresh the page on changing views. angular JS would be a good framework to use for this. 

5. I have used a generic repository pattern on the data layer as this can make coding simpler, and with less code saving time and keeping code in 1 place(Repository.cs), however we can also create specific repository interfaces and reference these through unity. I would expect that there would be a cache layer and a handler that the repository manages as part of data access.

6. I would typically like to create a proxy between the web ui and any other back end code, so that it is easy to switch out components if needed, such as a UI change from web to desktop etc, however to save time in this solution I added business logic into the controller. (I have added TODO: where I would like to add the proxy layer)

7. I would make the UI a lot nicer. But for this project with the time constraints I have not been able to focus too much on the UI style.

8. I have not implemented any authentication, I would expect this to be done in reality via SSO or active directory, with resource based authorization.

9. I have not implemented any error handling, there would be global error handler, and specific error handling in some areas where needed, but mostly we rely on adding good validation

10. I would dynamically load the filters on the dashboard from the database via an API, but for time I have hard coded the select boxes with 2 index names that are in the Data.csv file.

11. I would write more tests to cover all aspects of the code, web, domain etc. With this project, due to time I have only written a few calculation tests to test prices. 

Steps:
Please upload the file in the solution Data.csv, I have added another index named 'TEST2' to show 2 index levels on the chart, however the prices in the CSV are random. 

You will then see the data with weights in the Underlying Level Data view

You can then select Dashboard on the left to view the chart & pie chart, and filter by date and index on the line chart, and by index name on the pie chart. 