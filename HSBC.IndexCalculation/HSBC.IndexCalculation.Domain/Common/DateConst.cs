﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Common
{
    public class DateConst
    {
        public const string DateFormat = "dd MMM yyy";
    }
}
