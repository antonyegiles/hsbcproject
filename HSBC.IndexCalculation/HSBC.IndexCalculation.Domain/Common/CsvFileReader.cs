﻿using CsvHelper;
using HSBC.IndexCalculation.Domain.Entity;
using HSBC.IndexCalculation.Domain.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Common
{
    public class CsvFileReader : IFileReader
    {
        public IEnumerable<T> ReadAllLines<T>(Stream fileInputStream)
        {
            using (var reader = new StreamReader(fileInputStream))
            using (var csvReader = new CsvReader(reader))
            {
                while (csvReader.Read())
                {
                    yield return csvReader.GetRecord<T>();
                }
            }
        }
    }
}
