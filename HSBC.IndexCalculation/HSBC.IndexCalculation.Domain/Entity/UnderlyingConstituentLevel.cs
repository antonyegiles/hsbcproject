﻿using HSBC.IndexCalculation.Domain.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Entity
{
    public class UnderlyingConstituentLevel : EntityBase
    {
        public string IndexName { get; set; }
        public DateTime Date { get; set; }
        public string StockId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal NumberOfShares { get; set; }
        public decimal Weight { get; set; }
        public decimal WeightPrice { get; set; }
    }
}
