﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Entity.Base
{
    public class EntityBase
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }

        public void SetCreated()
        {
            Created = DateTime.Now;
        }
    }
}
