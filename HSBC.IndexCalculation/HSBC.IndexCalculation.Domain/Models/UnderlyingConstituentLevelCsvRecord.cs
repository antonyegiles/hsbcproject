﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Entity
{
    public class UnderlyingConstituentLevelCsvRecord
    {
        public string INDEX_NAME { get; set; }
        public DateTime DATE { get; set; }
        public string STOCK_ID { get; set; }
        public string NAME { get; set; }
        public decimal PRICE { get; set; }
        public decimal NUM_SHARES { get; set; }
    }
}
