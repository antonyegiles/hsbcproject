﻿using HSBC.IndexCalculation.Domain.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Calculation
{
    public class CaplitalizationWeightedIndexCalculationEngine : ICalculationEngine
    {
        public IEnumerable<Entity.IndexLevel> CalculateIndexLevels(IEnumerable<Entity.IndexLevel> Indices)
        {
            //TODO: validation?
            //TODO: logging calculating levels
            //TODO: error handling

            decimal? currentBasketPrice = null;
            //Hard coded for this project
            decimal currentLevel = Indices.Any(x => x.Name == "TEST2") ? 105M : 100M;
            foreach (var index in Indices.OrderBy(x => x.Date))
            {
                index.Basket =index.Basket.ToArray();

                var basketTotalPrice = (decimal)index.Basket.Sum(x => x.Price * x.NumberOfShares);

                foreach (var basketItem in index.Basket)
                {
                    basketItem.WeightPrice = basketItem.Price * (decimal)basketItem.NumberOfShares;
                    basketItem.Weight = basketItem.WeightPrice / basketTotalPrice * 100;
                }

                index.Price = currentBasketPrice.HasValue ?
                    currentLevel * (basketTotalPrice / currentBasketPrice.Value) :
                    currentLevel;
                currentBasketPrice = basketTotalPrice;
            }

            return Indices;
        }
    }
}
