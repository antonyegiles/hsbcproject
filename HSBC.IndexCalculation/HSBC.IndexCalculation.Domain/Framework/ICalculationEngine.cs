﻿using HSBC.IndexCalculation.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Framework
{
    public interface ICalculationEngine
    {
        IEnumerable<IndexLevel> CalculateIndexLevels(IEnumerable<IndexLevel> Indices);
    }
}
