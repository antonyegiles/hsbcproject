﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Framework
{
    public interface IRepositoryAsync<TEntity> : IRepository<TEntity> where TEntity : class
    {
    }
}
