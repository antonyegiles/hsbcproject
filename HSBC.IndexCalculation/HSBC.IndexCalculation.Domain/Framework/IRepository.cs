﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Framework
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Clear();
        void Insert(TEntity entity);
        void InsertMany(IEnumerable<TEntity> entitys);
        IQueryable<TEntity> GetAll();
    }
}
