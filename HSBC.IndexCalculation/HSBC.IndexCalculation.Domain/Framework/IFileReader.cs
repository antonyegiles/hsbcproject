﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Domain.Framework
{
    public interface IFileReader
    {
        IEnumerable<T> ReadAllLines<T>(Stream fileInputStream);
    }
}
