﻿using HSBC.IndexCalculation.Domain.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        //TODO: Implement sql server DB with EF6 context
        //TODO: Investget, do we need to add unit of work, or can we use 
        // EF built in functionality.
        private readonly static List<TEntity> _dbSet = new List<TEntity>();

        public void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsQueryable();
        }

        public void Clear()
        {
            _dbSet.Clear();
        }


        public void InsertMany(IEnumerable<TEntity> entitys)
        {
            foreach(var item in entitys)
                Insert(item);
        }
    }
}
