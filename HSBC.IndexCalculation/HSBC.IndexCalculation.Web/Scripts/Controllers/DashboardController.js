﻿$(function () {
    refreshLineChart();
    refreshPieChart();
});

function createLineChart(result) {
    $('#line-chart-container').highcharts({
        title: {
            text: 'Index Levels Per Date',
            x: -20 //center
        },
        xAxis: {
            categories: result.Dates
        },
        yAxis: {
            title: {
                text: 'Level'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: result.Series
    });
}
function createPieChart(result) {
    debugger;
    $('#pie-chart-container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '% Of Top 5 Weighted Stocks'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b><br/> {point.percentage:.1f} % of top 5',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Stocks",
            colorByPoint: true,
            data: result
        }]
    });
}
function refreshLineChart() {
    $.ajax({
        url: "/api/IndexLevel/GetIndexLevelsByDate?dateFrom=" + $("#date-from").val() + "&dateTo=" + $("#date-to").val() + '&indexName=' + $("#indexNameSelect").val(),
        success: function (result) {
            createLineChart(result);
        }
    });
}
function refreshPieChart() {
    $.ajax({
        url: "/api/TopPerformingStocks?indexName=" + $("#indexNamePieSelect").val(),
        success: function (result) {
            createPieChart(result);
        }
    });
}