﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HSBC.IndexCalculation.Web.Models
{
    public class GetIndexLevelsByDateModel
    {
        public IEnumerable<string> Dates { get; set; }
        public IEnumerable<object> Series { get; set; }
    }
}