﻿using HSBC.IndexCalculation.Data.Repository;
using HSBC.IndexCalculation.Domain.Calculation;
using HSBC.IndexCalculation.Domain.Common;
using HSBC.IndexCalculation.Domain.Entity;
using HSBC.IndexCalculation.Domain.Framework;
using HSBC.IndexCalculation.Web.App_Start;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HSBC.IndexCalculation.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<IFileReader, CsvFileReader>();
            container.RegisterType<IRepository<IndexLevel>, Repository<IndexLevel>>();
            container.RegisterType<ICalculationEngine, CaplitalizationWeightedIndexCalculationEngine>();
            config.DependencyResolver = new UnityResolver(container);
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "SpecificApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
