﻿using HSBC.IndexCalculation.Domain.Entity;
using HSBC.IndexCalculation.Domain.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HSBC.IndexCalculation.Web.api
{
    public class TopPerformingStocksController : ApiController
    {
        private readonly IRepository<IndexLevel> _indexLevelRepository;
        public TopPerformingStocksController(IRepository<IndexLevel> indexLevelRepository)
        {
            _indexLevelRepository = indexLevelRepository;
        }

        public HttpResponseMessage Get(string indexName)
        {
            var topPerformingStocks = _indexLevelRepository.GetAll()
                .Where(x => x.Name == indexName)
                .SelectMany(x => x.Basket)
                .OrderByDescending(x => x.Date)
                .ThenByDescending(x => x.Weight)
                .Take(5).ToList();
            var sumOfTopWeights = topPerformingStocks.Sum(y => y.Weight);
            return Request.CreateResponse(HttpStatusCode.OK, topPerformingStocks.Select(x =>
                new
                {
                    name = x.Name + " - " + Math.Round(x.Weight, 2) + " basket %",
                    y = (float)(x.Weight / sumOfTopWeights) * 100
                }));
        }
    }
}