﻿using HSBC.IndexCalculation.Domain.Common;
using HSBC.IndexCalculation.Domain.Entity;
using HSBC.IndexCalculation.Domain.Framework;
using HSBC.IndexCalculation.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HSBC.IndexCalculation.Web.api
{
    public class IndexLevelController : ApiController
    {
        private readonly IRepository<IndexLevel> _indexLevelRepository;
        public IndexLevelController(IRepository<IndexLevel> indexLevelRepository)
        {
            _indexLevelRepository = indexLevelRepository;
        }

        public IEnumerable<IndexLevel> Get()
        {
            return _indexLevelRepository.GetAll().OrderBy(x => x.Name).ThenByDescending(x => x.Date);
        }

        [ActionName("GetIndexLevelsByDate")]
        public GetIndexLevelsByDateModel GetIndexLevelsByDate(DateTime? dateFrom, DateTime? dateTo, string indexName)
        {
            if (dateFrom == null) dateFrom = DateTime.Now.AddYears(-1);
            if (dateTo == null) dateTo = DateTime.Now;

            var indexLevels = _indexLevelRepository.GetAll()
                .Where(x => x.Date >= dateFrom && x.Date <= dateTo && (indexName == null || indexName==x.Name))
                .OrderBy(x => x.Date);

            var model = new GetIndexLevelsByDateModel();

            if (!indexLevels.Any())
                return model;

            model.Dates = indexLevels.Select(x => x.Date.ToString(DateConst.DateFormat)).Distinct();
            model.Series = indexLevels.GroupBy(x => x.Name).Select(x => new { name = x.Key, data = x.Select(y => y.Price) });

            return model;
        }
    }
}