﻿using HSBC.IndexCalculation.Domain.Entity;
using HSBC.IndexCalculation.Domain.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace HSBC.IndexCalculation.Web.api
{
    public class FileUploadController : ApiController
    {
        private readonly IFileReader _fileReader;
        private readonly IRepository<IndexLevel> _indexLevelRepository;
        private readonly ICalculationEngine _calculationEngine;
        public FileUploadController(IFileReader fileReader, IRepository<IndexLevel> indexLevelRepository,
            ICalculationEngine calculationEngine)
        {
            _fileReader = fileReader;
            _indexLevelRepository = indexLevelRepository;
            _calculationEngine = calculationEngine;
        }
        
        public void Post()
        {
            //TODO: implement the service pattern to create a proxy between api controller
            //and business logic. This would have insert/update & validation logic
            var file = HttpContext.Current.Request.Files.Count > 0 ?
            HttpContext.Current.Request.Files[0] : null;

            if (file != null && file.ContentLength > 0)
            {
                var csvRecords = _fileReader.ReadAllLines<UnderlyingConstituentLevelCsvRecord>(file.InputStream);

                var group = csvRecords
                .GroupBy(x => new { x.INDEX_NAME, x.DATE });
                var indexLevels = group.Select(x => new IndexLevel
                {
                    Basket = x.Select(uc => new UnderlyingConstituentLevel 
                    {
                        IndexName = uc.INDEX_NAME,
                        Date = uc.DATE,
                        StockId = uc.STOCK_ID,
                        Price = uc.PRICE,
                        NumberOfShares = uc.NUM_SHARES,
                        Name = uc.NAME
                    }),
                    Date = x.Key.DATE,
                    Name = x.Key.INDEX_NAME
                }).ToList().AsEnumerable();

                //TODO: move this logic into the service behind proxy
                foreach (var indexGroup in indexLevels.GroupBy(x => x.Name))
                {
                    var items = indexGroup.Select(x => x);
                    indexLevels = _calculationEngine.CalculateIndexLevels(items);
                    _indexLevelRepository.InsertMany(items);
                }
            }
        }
    }
}