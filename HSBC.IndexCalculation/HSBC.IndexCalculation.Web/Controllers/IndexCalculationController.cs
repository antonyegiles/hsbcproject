﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HSBC.IndexCalculation.Web.Controllers
{
    public class IndexCalculationController : Controller
    {
        public ActionResult UploadLevels()
        {
            return View();
        }

        public ActionResult IndexLevelData()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }
    }
}