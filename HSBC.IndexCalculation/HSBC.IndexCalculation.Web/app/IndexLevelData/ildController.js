﻿indexCalculationApp.controller('ildController',
function ($scope, ildService) {
    var vm = this;
    vm.indexLevels = [];

    vm.getIndexLevels = function () {
        ildService.getIndexLevels()
            .then(function (indexLevels) {
                vm.indexLevels = indexLevels;
                console.log('index Levels returned to controller.');
            },
            function (data) {
                console.log('index Levels retrieval failed.')
            });
    };

    vm.getIndexLevels();
});
