﻿indexCalculationApp.factory('ildService',
function ($http, $q) {
    // interface
    var service = {
        indexLevels: [],
        getIndexLevels: getIndexLevels
    };
    return service;

    // implementation
    function getIndexLevels() {
        var def = $q.defer();

        $http.get("/api/IndexLevel")
            .success(function (data) {
                service.albums = data;
                def.resolve(data);
            })
            .error(function () {
                def.reject("Failed to get index Levels");
            });
        return def.promise;
    }
});

