﻿using HSBC.IndexCalculation.Domain.Calculation;
using HSBC.IndexCalculation.Domain.Entity;
using HSBC.IndexCalculation.Domain.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSBC.IndexCalculation.Tests.Domain
{
    [TestClass]
    public class CalculationEngineTests
    {
        [TestMethod]
        public void TestCalculationWeightPricesAreCalculated()
        {
            //Arrange
            ICalculationEngine calcEngine = new CaplitalizationWeightedIndexCalculationEngine();
            List<IndexLevel> indexLevels = new List<IndexLevel> 
                { 
                    new IndexLevel 
                    {
                        Name = "TEST1",
                        Date = new DateTime(2015, 01, 01),
                        Basket = new List<UnderlyingConstituentLevel>
                        {
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 2, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 3, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 2},
                        }
                    } 
                };
            
            //Act
            calcEngine.CalculateIndexLevels(indexLevels);

            //Assert
            Assert.AreEqual(1, indexLevels[0].Basket.First().WeightPrice);
            //TODO: write tests to varify the remaining stocks 
            //in the basket have calculated correctly
        }

        [TestMethod]
        public void TestCalculationWeightPercentageAreCalculatedByDate()
        {
            //Arrange
            ICalculationEngine calcEngine = new CaplitalizationWeightedIndexCalculationEngine();
            List<IndexLevel> indexLevels = new List<IndexLevel> 
                { 
                    new IndexLevel 
                    {
                        Name = "TEST1",
                        Date = new DateTime(2015, 01, 01),
                        Basket = new List<UnderlyingConstituentLevel>
                        {
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 2, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 3, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 2},
                        }
                    } 
                };

            //Act
            calcEngine.CalculateIndexLevels(indexLevels);

            //Assert
            Assert.AreEqual(12.5M, indexLevels[0].Basket.First().Weight);
            //TODO: write tests to varify the remaining stocks 
            //in the basket have calculated correctly
        }

        [TestMethod]
        public void TestIndexCalculation()
        {
            //Arrange
            ICalculationEngine calcEngine = new CaplitalizationWeightedIndexCalculationEngine();
            List<IndexLevel> indexLevels = new List<IndexLevel> 
                { 
                    new IndexLevel 
                    {
                        Name = "TEST1",
                        Date = new DateTime(2015, 01, 01),
                        Basket = new List<UnderlyingConstituentLevel>
                        {
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 2, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 3, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 2},
                        }
                    },
                    new IndexLevel 
                    {
                        Name = "TEST1",
                        Date = new DateTime(2015, 01, 02),
                        Basket = new List<UnderlyingConstituentLevel>
                        {
                            new UnderlyingConstituentLevel { Price = 1, NumberOfShares = 2},
                            new UnderlyingConstituentLevel { Price = 3, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 3, NumberOfShares = 1},
                            new UnderlyingConstituentLevel { Price = 1.5M, NumberOfShares = 2},
                        }
                    } 
                };

            //Act
            calcEngine.CalculateIndexLevels(indexLevels);

            // Assert
            // 01 01 2015 = 8
            // 02 01 2015 = 11
            // 11/8 = 1.375 = 37.5%
            // Initial price = 100 new price = 137.5
            Assert.AreEqual(137.5M, indexLevels[1].Price);

            //TODO: write more test with bigger baskets and more dates
        }
    }
}
